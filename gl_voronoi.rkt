#lang typed/racket
(require typed/opengl)
(require typed/racket/gui)
(require (except-in typed/opengl/ffi -> cast))

(define vertices
  (f32vector -1.0 1.0
             -1.0 -1.0
             1.0 -1.0

             -1.0 1.0
             1.0 -1.0
             1.0 1.0))

(define points
  (f32vector -0.6 0.5
             -0.3 -0.2
             0.7 0.1
             0.2 -0.4
             -0.7 -0.3))

(define colors
  (f32vector 1.0 0.0 0.0
             0.0 1.0 0.0
             0.0 0.0 1.0
             1.0 1.0 0.0
             1.0 1.0 1.0))

(: resize (-> Integer Integer Void))
(define (resize w h)
  (glViewport 0 0 w h))

(struct gl-state
  ([program : Nonnegative-Integer]
   [vertex-buffer : Integer]
   [vertex-array-object : Integer]))

(define (draw-opengl)
  (glClearColor 0.0 0.0 0.0 0.0)
  (glClear GL_COLOR_BUFFER_BIT)
  (glDrawArrays GL_TRIANGLES 0 6))

(: init-graphics (-> gl-state))
(define (init-graphics)
  (printf "OpenGL version: ~a\n" (gl-version))
  (let ([prog (create-program (load-shader "gl_voronoi.frag"
                                           GL_FRAGMENT_SHADER)
                              (load-shader "gl_voronoi.vert"
                                           GL_VERTEX_SHADER))]
        [vertex-buffer (u32vector-ref (glGenBuffers 1) 0)]
        [vertex-array-object (u32vector-ref (glGenVertexArrays 1) 0)])
    (glBindVertexArray vertex-array-object)
    (glBindBuffer GL_ARRAY_BUFFER vertex-buffer)
    (glBufferData GL_ARRAY_BUFFER
                  (* (gl-type-sizeof GL_FLOAT) (f32vector-length vertices))
                  (f32vector->cpointer vertices)
                  GL_STATIC_DRAW)
    (glEnableVertexAttribArray 0)
    (glVertexAttribPointer 0 2 GL_FLOAT #f 0 0)

    (gl-state prog vertex-buffer vertex-array-object)))

(define gl-conf (new gl-config%))
(send gl-conf set-legacy? #f)

(define my-canvas%
  (class canvas%
    (inherit with-gl-context swap-gl-buffers)

    (: gl-vars (Option gl-state))
    (define gl-vars #f)

    (super-instantiate () (style '(gl)) (gl-config gl-conf))

    (: on-paint (-> Any))
    (define/override (on-paint)
      (with-gl-context
        (lambda ()
          (unless gl-vars
            (let* ([vars (init-graphics)]
                   [prog (gl-state-program vars)])
              (set! gl-vars vars)
              (glUseProgram prog)
              (glUniform2fv (glGetUniformLocation prog "points") 5 points)
              (glUniform3fv (glGetUniformLocation prog "colors") 5 colors)))
          (draw-opengl)
          (swap-gl-buffers))))

    (: on-size (-> Integer Integer Any))
    (define/override (on-size width height)
      (with-gl-context (lambda ()
                         (resize width height))))))

(define win (new frame% [label "OpenGL voronoi"]
                 [min-width 200] [min-height 200]))

(define gl (new my-canvas% [parent win]))

(send win show #t)

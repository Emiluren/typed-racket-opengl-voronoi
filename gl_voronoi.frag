#version 330 core
in vec2 vPos;
out vec4 FragColor;

const int NUM_POINTS = 5;
uniform vec2 points[NUM_POINTS];
uniform vec3 colors[NUM_POINTS];

void main()
{
    float closest_distance = 10;
    vec3 closest_color;
    for (int i = 0; i < NUM_POINTS; i++) {
        float new_dist = distance(points[i], vPos);
        if (new_dist < closest_distance) {
            closest_distance = new_dist;
            closest_color = colors[i];
        }
    }
    FragColor = vec4(closest_color, 1.0f);
}
